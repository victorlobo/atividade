package com.example.atividade;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnCalcular = (Button) findViewById(R.id.bt_calcular);
        btnCalcular.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                DecimalFormat df = new DecimalFormat();
                df.setMaximumFractionDigits(2);
                df.setMinimumFractionDigits(2);

                TextView in_consumo = (TextView)findViewById(R.id.edt_consumo);
                TextView in_couver_artistico = (TextView)findViewById(R.id.edt_couver_artistico);
                TextView in_dividir = (TextView)findViewById(R.id.edt_dividir);
                TextView in_servico = (TextView)findViewById(R.id.edt_servico);
                TextView in_conta_total = (TextView)findViewById(R.id.etd_conta_total);
                TextView in_valor_pessoa = (TextView)findViewById(R.id.etd_valor_pessoa);

                float conta_total = Float.parseFloat(in_consumo.getText().toString()) + Float.parseFloat(in_couver_artistico .getText().toString());
                float servico = conta_total / 10;
                conta_total += servico;
                float valor_pessoa = conta_total / Float.parseFloat(in_dividir.getText().toString());
                in_servico.setText(df.format(servico));
                in_conta_total.setText(df.format(conta_total));
                in_valor_pessoa.setText(df.format(valor_pessoa));
            }
        });
    }
}
